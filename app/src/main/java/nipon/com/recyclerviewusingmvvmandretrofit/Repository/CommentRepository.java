package nipon.com.recyclerviewusingmvvmandretrofit.Repository;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.widget.Toast;

import java.util.List;

import nipon.com.recyclerviewusingmvvmandretrofit.Interface.CommentInterface;
import nipon.com.recyclerviewusingmvvmandretrofit.Model.Comment;
import nipon.com.recyclerviewusingmvvmandretrofit.Retrofit.APIService;
import nipon.com.recyclerviewusingmvvmandretrofit.Retrofit.RetrofitInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class CommentRepository implements CommentInterface {

    private Application application;

    private MutableLiveData<List<Comment>> commentList;
    private MutableLiveData<Boolean> isLoaded;

    public CommentRepository(Application application) {
        this.application = application;
        getDataFromDB();
    }



    private void getDataFromDB() {
        commentList = new MutableLiveData<>();
        isLoaded = new MutableLiveData<>();

        Retrofit retrofit = RetrofitInstance.getRetrofitInstance();
        APIService apiService = retrofit.create(APIService.class);

        Call<List<Comment>> call = apiService.getComments(3);

        call.enqueue(new Callback<List<Comment>>() {
            @Override
            public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {
                if (!response.isSuccessful()){
                    return;
                }

                Toast.makeText(application, "OnResponse", Toast.LENGTH_SHORT).show();
                List<Comment> comments = response.body();
                commentList.setValue(comments);
                isLoaded.setValue(true);
            }

            @Override
            public void onFailure(Call<List<Comment>> call, Throwable t) {
                Toast.makeText(application, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public MutableLiveData<List<Comment>> getAllComment() {
        return commentList;
    }

    @Override
    public MutableLiveData<Boolean> getIsLoaded() {
        return isLoaded;
    }
}
