package nipon.com.recyclerviewusingmvvmandretrofit.Retrofit;

import java.util.List;

import nipon.com.recyclerviewusingmvvmandretrofit.Model.Comment;
import nipon.com.recyclerviewusingmvvmandretrofit.Model.Post;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIService {

    @GET("posts")
    Call<List<Post>> getPosts();

    @GET("posts")
    Call<List<Post>> getPosts(
            @Query("userId") Integer userId,
            @Query("_sort") String sort,
            @Query("_order") String order
    );


    @POST("posts")
    Call<Post> createPost(@Body Post post);


    @GET("comments")
    Call<List<Comment>> getComments(@Query("postId") int postId);

}
