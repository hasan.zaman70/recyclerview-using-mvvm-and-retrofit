package nipon.com.recyclerviewusingmvvmandretrofit.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;


import java.util.List;

import nipon.com.recyclerviewusingmvvmandretrofit.Interface.CommentInterface;
import nipon.com.recyclerviewusingmvvmandretrofit.Model.Comment;
import nipon.com.recyclerviewusingmvvmandretrofit.Repository.CommentRepository;

public class CommentViewModel extends AndroidViewModel implements CommentInterface {

    private MutableLiveData<List<Comment>> commentList;
    private MutableLiveData<Boolean> isLoaded;

    public CommentViewModel(Application application) {
        super(application);
        if (commentList!=null){
            return;
        }
        CommentRepository commentRepository = new CommentRepository(application);
        commentList = commentRepository.getAllComment();
        isLoaded = commentRepository.getIsLoaded();
    }


    @Override
    public MutableLiveData<List<Comment>> getAllComment() {
        return commentList;
    }

    @Override
    public MutableLiveData<Boolean> getIsLoaded() {
        return isLoaded;
    }
}
