package nipon.com.recyclerviewusingmvvmandretrofit.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;


import java.util.List;

import nipon.com.recyclerviewusingmvvmandretrofit.Model.Post;
import nipon.com.recyclerviewusingmvvmandretrofit.Repository.PostRepository;

public class PostViewModel extends AndroidViewModel {

    private MutableLiveData<List<Post>> postList;
    private MutableLiveData<Boolean> isLoaded;
    private PostRepository postRepository;

    public PostViewModel( Application application) {
        super(application);
        if (postList != null) {
            return;
        }
        postRepository = new PostRepository(application);
        postList = postRepository.getAllPost();
        isLoaded = postRepository.getIsLoaded();
    }

    public LiveData<List<Post>> getAllPost() {
        return postList;
    }

    public MutableLiveData<Boolean> getIsLoaded() {
        return isLoaded;
    }

}
